const express = require('express');
const router = express.Router();
const ctrlStaff = require('../controllers/staff');
const ctrlUserDetails = require('../controllers/user_details');

router.route('/staff')
  .get(ctrlStaff.staffSimpleList)
  .post(ctrlStaff.staffCreate);

router.route('/staff/:staff_id')
  .get(ctrlStaff.getSelectedStaff)
  .put(ctrlStaff.updateSelectedStaff)
  .delete(ctrlStaff.deleteSelectedStaff);

router.route('/nation')
  .get(ctrlUserDetails.nationList);
  
router.route('/local_state')
  .get(ctrlUserDetails.localStateList);

router.route('/gender')
  .get(ctrlUserDetails.genderList);

router.route('/religion')
  .get(ctrlUserDetails.religionList);

router.route('/race')
  .get(ctrlUserDetails.raceList);

router.route('/marriage_status')
  .get(ctrlUserDetails.marriageStatusList);

router.route('/employee_status')
  .get(ctrlUserDetails.employeeStatusList);

router.route('/staff_position')
  .get(ctrlStaff.staffPositionList);

router.route('/office_dept')
  .get(ctrlStaff.officeDeptList);

router.route('/staff_contract')
  .post(ctrlStaff.createContract);

router.route('/staff_contract/:staff_id')
  .get(ctrlStaff.getContractOfStaff);

router.route('/staff_contract_single/:_id')
  .get(ctrlStaff.getSingleContract)
  .put(ctrlStaff.updateSingleContract);

router.route('/staffs_with_latest_contract')
  .get(ctrlStaff.getStaffWithLatestContract);

router.route('/payslip')
  .get(ctrlStaff.getPayslips)
  .post(ctrlStaff.createPayslips);

module.exports = router;