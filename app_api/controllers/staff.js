const mongoose = require('mongoose');
const Staff = mongoose.model('Staff');
const StaffPosition = mongoose.model('StaffPosition');
const OfficeDept = mongoose.model('OfficeDept');
const StaffContract = mongoose.model('StaffContract');
const Payslip = mongoose.model('Payslip');

const staffSimpleList = (req, res) => {
    Staff.find({}, async (err, data) => {
        if (err) {
            return res
                .status(404)
                .json(err);
        }

        if (data.length == 0) {
            return res
                .status(404)
                .json({"message": "data not found"});
        }

        let result = [];
        let i = 1;

        for (let elem of data) {
            let position = await
                StaffPosition.findById({
                    _id: elem.position_id
                }).exec()
                    .then(val => {
                        return {
                            gred: val.gred,
                            jawatan: val.jawatan
                        }
                    });

            let department = await
                OfficeDept.findById({
                    _id: elem.dept_id
                }).exec()
                    .then(val => {
                        return {
                            pusat: val.pusat,
                            bahagian: val.bahagian
                        }
                    });

            let temp = {
                _id: elem._id,
                count: i,
                name: elem.name,
                gred: position.gred,
                jawatan: position.jawatan,
                pusat: department.pusat,
                bahagian: department.bahagian
            };

            result.push(temp);
            i++;
        }

        return res
            .status(200)
            .json(result);
    });
}


const getSelectedStaff = (req, res) => {
    Staff.findById(req.params.staff_id, async (err, data) => {
        if (err) {
            return res
                .status(404)
                .json(err);
        }

        if (data.length == 0) {
            return res
                .status(404)
                .json({"message": "data not found"});
        }

        return res
            .status(200)
            .json(data);
    });
}

const updateSelectedStaff = (req, res) => {
    Staff.findById(req.params.staff_id, async (err, staff) => {        
        if (!staff) {
            return res
            .status(404)
            .json({"message": "data not found"});
        }

        if (err) {
            return res
                .status(400)
                .json(err);
        }

        staff.position_id = req.body.position_id;
        staff.dept_id = req.body.dept_id;
        staff.name = req.body.name;
        staff.contact_no = req.body.contact_no;
        staff.secondary_email = req.body.secondary_email;
        staff.home_address = req.body.home_address;
        staff.postcode = req.body.postcode;
        staff.IC = req.body.IC;
        staff.nation_id = req.body.nation_id;
        staff.gender_id = req.body.gender_id;
        staff.religion_id = req.body.religion_id;
        staff.marriage_status_id = req.body.marriage_status_id;
        staff.DOB = req.body.DOB;
        staff.StateOB_id = req.body.StateOB_id;
        staff.race_id = req.body.race_id;
        staff.date_enter = req.body.date_enter;

        staff.save((err, staff) => {
            if (err) {
                res
                    .status(404)
                    .json(err);
            }
            else {
                res
                    .status(200)
                    .json(staff);
            }
        });
    });
}

const staffPositionList = (req, res) => {
    StaffPosition.find({}, (err, data) => {
        if (err) {
            return res
                .status(404)
                .json(err);
        }

        if (data.length == 0) {
            return res
                .status(404)
                .json({"message": data});
        }

        return res
            .status(200)
            .json(data);
    });
}

const officeDeptList = (req, res) => {
    OfficeDept.find({}, (err, data) => {
        if (err) {
            return res
                .status(404)
                .json(err);
        }

        if (data.length == 0) {
            return res
                .status(404)
                .json({"message": data});
        }

        return res
            .status(200)
            .json(data);
    });
}

const staffCreate = (req, res) => {
    Staff.create({
        position_id: req.body.position_id,
        dept_id: req.body.dept_id,
        name: req.body.name,
        contact_no: req.body.contact_no,
        secondary_email: req.body.secondary_email,
        home_address: req.body.home_address,
        postcode: req.body.postcode,
        IC: req.body.IC,
        nation_id: req.body.nation_id,
        gender_id: req.body.gender_id,
        religion_id: req.body.religion_id,
        marriage_status_id: req.body.marriage_status_id,
        DOB: req.body.DOB,
        StateOB_id: req.body.StateOB_id,
        race_id: req.body.race_id,
        date_enter: req.body.date_enter,
        employee_status_id: req.body.employee_status_id,
    },
    (err, data) => {
        if (err) {
            res
                .status(400)
                .json(err);
        }
        else {
            res
                .status(201)
                .json(data);
        }
    });
}

const deleteSelectedStaff = (req, res) => {
    Staff.findByIdAndDelete(req.params.staff_id, async (err, staff) => {        
        if (!staff) {
            return res
            .status(404)
            .json({"message": "data not found"});
        }

        if (err) {
            return res
                .status(400)
                .json(err);
        }

        res
            .status(200)
            .json(staff);
    });
}

const createContract = (req, res) => {
    StaffContract.create({
        staff_id: req.body.staff_id,
        date_start: req.body.date_start,
        date_end: req.body.date_end,
        status: req.body.status,
        salary: req.body.salary
    },
    (err, data) => {
        if (err) {
            res
                .status(400)
                .json(err);
        }
        else {
            res
                .status(201)
                .json(data);
        }
    })
}

const getContractOfStaff = (req, res) => {
    StaffContract.find(
        { staff_id: req.params.staff_id}, 
        null,
        { sort: {date_start: -1}},
        async (err, data) => {
        if (err) {
            return res
                .status(404)
                .json(err);
        }

        if (data.length == 0) {
            return res
                .status(404)
                .json({"message": "data not found"});
        }

        return res
            .status(200)
            .json(data);
    });
}

const getSingleContract = (req, res) => {
    StaffContract.find({ _id: req.params._id}, async (err, data) => {
        if (err) {
            return res
                .status(404)
                .json(err);
        }

        if (data.length == 0) {
            return res
                .status(404)
                .json({"message": "data not found"});
        }

        return res
            .status(200)
            .json(data);
    });
}

const updateSingleContract = (req, res) => {
    StaffContract.findById(req.params._id, async (err, contract) => {        
        if (!contract) {
            return res
            .status(404)
            .json({"message": "data not found"});
        }

        if (err) {
            return res
                .status(400)
                .json(err);
        }

        contract.status = req.body.status;
        contract.salary = req.body.salary;

        contract.save((err, staff) => {
            if (err) {
                res
                    .status(404)
                    .json(err);
            }
            else {
                res
                    .status(200)
                    .json(contract);
            }
        });
    });
}

const getStaffWithLatestContract = (req, res) => {
    // get all staffs

    let result = [];

    Staff.find({}, async (err, data) => {
        if (err) {
            return res
                .status(404)
                .json(err);
        }

        if (data.length == 0) {
            return res
                .status(404)
                .json({"message": "data not found"});
        }
        let i = 1;

        for (let elem of data) {
            let position = await
                StaffPosition.findById({
                    _id: elem.position_id
                }).exec()
                    .then(val => {
                        return {
                            gred: val.gred,
                            jawatan: val.jawatan
                        }
                    });

            let latestContract = await
                StaffContract.find({
                    staff_id: elem._id,
                })
                    .sort({date_start: -1})
                    .then(val => val[0]);

            if (latestContract != null) {
                let temp = {
                    _id: elem._id,
                    count: i,
                    name: elem.name,
                    gred: position.gred,
                    jawatan: position.jawatan,
                    basic: latestContract.salary
                };
                result.push(temp);
                i++;
            }
        }
        
        return res
            .status(200)
            .json(result);
    })
}

const createPayslips = (req, res) => {
    Payslip.insertMany(req.body
        ,(err, data) => {
            if (err) {
                res
                    .status(400)
                    .json(err);
            }
            else {
                res
                    .status(201)
                    .json(data);
            }
        }
    )
}

const getPayslips = (req, res) => {
    Payslip.find({}, (err, data) => {
        if (err) {
            res
                .status(404)
                .json(err);
        }
        else {
            res
                .status(200)
                .json(data);
        }
    })
}

module.exports = {
    staffSimpleList,
    staffPositionList,
    officeDeptList,
    staffCreate,
    getSelectedStaff,
    updateSelectedStaff,
    deleteSelectedStaff,
    createContract,
    getContractOfStaff,
    getSingleContract,
    updateSingleContract,
    getStaffWithLatestContract,
    createPayslips,
    getPayslips
}