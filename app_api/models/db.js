const mongoose = require('mongoose');
const dbURI = 'mongodb://localhost/staff_module';
mongoose.connect(dbURI, {
    useNewUrlParser: true,
    useUnifiedTopology: true 
});
mongoose.connection.on('connected', () => {
 console.log(`Mongoose connected to ${dbURI}`);
});

require('./staff');
require('./user');
require('./user_details');