const mongoose = require('mongoose');

const staffSchema = new mongoose.Schema({
    user_id: {
        type: String,
        // required: true
    },
    position_id: {
        type: String,
        required: true
    },
    dept_id: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    contact_no: {
        type: String,
        required: true
    },
    secondary_email: {
        type: String,
        required: true
    },
    home_address: {
        type: String,
        required: true
    },
    postcode: {
        type: String,
        required: true
    },
    IC: {
        type: String,
        required: true
    },
    nation_id: {
        type: String,
        required: true
    },
    gender_id: {
        type: String,
        required: true
    },
    religion_id: {
        type: String,
        required: true
    },
    marriage_status_id: {
        type: String,
        required: true
    },
    // disability_id: {
    //     type: String
    // },
    DOB: {
        type: Date,
        required: true
    },
    StateOB_id: {
        type: String,
        required: true
    },
    race_id: {
        type: String,
        required: true
    },
    date_enter: {
        type: Date,
        required: true
    },
    employee_status_id: {
        type: String,
        required: true
    },
});

const staffPositionSchema = new mongoose.Schema({
    gred: {
        type: String,
        required: true
    },
    jawatan: {
        type: String,
        required: true
    }
});

const officeDeptSchema = new mongoose.Schema({
    pusat: {
        type: String,
        required: true
    },
    bahagian: {
        type: String,
        required: true
    }
});

const payslipSchema = new mongoose.Schema({
    _id: {
        type: String,
        required: false
    },
    staff_id: {
        type: String,
        required: true
    },
    basic: {
        type: Number,
        required: true
    },
    date_issued: {
        type: Date,
        required: true,
        default: new Date()
    },
    additional_items: {
        type: Object,
        required: false
    }
})

const staffContractSchema = new mongoose.Schema({
    staff_id: {
        type: String,
        required: true
    },
    date_start: {
        type: Date,
        required: true
    },
    date_end: {
        type: Date,
        required: true
    },
    status: {
        type: Boolean,
        required: true,
        default: true
    },
    salary: {
        type: Number,
        required: true
    }
})

mongoose.model('Staff', staffSchema, 'Staff');
mongoose.model('StaffPosition', staffPositionSchema, 'StaffPosition');
mongoose.model('OfficeDept', officeDeptSchema, 'OfficeDept');
mongoose.model('Payslip', payslipSchema, 'Payslip');
mongoose.model('StaffContract', staffContractSchema, 'StaffContract');